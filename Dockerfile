FROM python:3.6
ENV PYTHONBUFFERED 1

RUN mkdir /code
ADD backend /code/

WORKDIR /code
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN mkdir -p /frontend
ADD ./frontend /frontend
WORKDIR /frontend
RUN curl -sL https://deb.nodesource.com/setup_12.x > /install_node.sh
RUN bash /install_node.sh
RUN apt-get install nodejs
ENV PATH="/frontend/node_modules/.bin:${PATH}"

WORKDIR  /frontend/tram-manager
RUN npm install
RUN npm install -g @angular/cli

WORKDIR /code
RUN echo "yes"|python3 manage.py collectstatic
VOLUME /code/static

EXPOSE 4200
EXPOSE 8001