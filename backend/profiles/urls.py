from profiles.api.user_views import UserListView, UserDetailAPIView
from rest_framework.routers import SimpleRouter
from django.urls import path

profile_router = SimpleRouter()
profile_router.register('users', UserDetailAPIView, basename='users')

urlpatterns = [
    path('users/', UserListView.as_view())
]
