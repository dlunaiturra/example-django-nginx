from rest_framework import serializers
from django.contrib.auth.models import User, Group
from profiles.serializers.profile_serializer import ProfileSerializer


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

    def to_representation(self, obj):
        return obj.name


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)
    profile = ProfileSerializer()
    complete_name = serializers.SerializerMethodField()

    def get_complete_name(self, obj):
        complete_name = obj.first_name + ' ' + obj.last_name
        return complete_name

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'profile', 'complete_name', 'groups')


class SimpleUserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()
    complete_name = serializers.SerializerMethodField()

    def get_complete_name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'profile', 'complete_name')
