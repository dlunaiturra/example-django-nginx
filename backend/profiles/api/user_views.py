from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets
from django.contrib.auth.models import User
from rest_framework.response import Response

from profiles.serializers.user_serializer import UserSerializer


class UserListView(generics.ListAPIView):
    queryset = User.objects.filter(first_name__isnull=False).all()
    serializer_class = UserSerializer
    pagination_class = None


class UserDetailAPIView(generics.RetrieveAPIView, viewsets.ViewSet):
    queryset = User.objects.filter(first_name__isnull=False).all()
    serializer_class = UserSerializer
    pagination_class = None

    def retrieve(self, request, pk=None, **kwargs):
        """
            If provided 'pk' is "me" then return the current user.
        """
        if request.user and pk == 'me':
            return Response(UserSerializer(request.user).data)
        else:
            return super(UserDetailAPIView, self).retrieve(request, pk)

    def list(self, request, *args, **kwargs):
        return Response(UserSerializer(self.queryset, many=True).data)
