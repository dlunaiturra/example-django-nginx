from django.contrib.auth import authenticate, login
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.status import (
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)


class Login(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request):
        data = request.data
        username = data['username']
        password = data['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            token, _ = Token.objects.get_or_create(user=user)
            result = {
                'token': token.key,
                'user_id': user.pk,
                'email': user.email
            }
            response = Response(result, status=HTTP_200_OK)
        else:
            response = Response({'error': 'Invalid Credentials'}, status=HTTP_404_NOT_FOUND)
        return response