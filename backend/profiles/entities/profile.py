from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Profile(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE,
                                related_name='profile',
                                verbose_name='Usuario')
    phone = models.CharField(max_length=16,
                             verbose_name='Teléfono',
                             null=True,
                             blank=True)
    mobile = models.CharField(max_length=16,
                              verbose_name='Móvil',
                              null=True,
                              blank=True)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


post_save.connect(create_user_profile, sender=User)
