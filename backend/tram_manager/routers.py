from rest_framework import routers


class CustomRouter(routers.DefaultRouter):
    def extend(self, router):
        """
            Extend the routes with url routes of the passed in router.
        Args:
             router: SimpleRouter instance containing route definitions.
        """
        self.registry.extend(router.registry)
