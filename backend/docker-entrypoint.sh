#!/bin/bash

cd /code
echo "yes"|python3 manage.py collectstatic
python3 manage.py makemigrations
python3 manage.py migrate
python3 -u manage.py runserver 0.0.0.0:8001