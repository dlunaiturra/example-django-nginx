from rest_framework import serializers
from claims.models import DischargeLocationClaimant, DischargeLocation
from claims.serializers.location_serializer import LocationSerializer
from claims.serializers.station_serializer import StationSerializer

class DischargeLocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = DischargeLocation
        fields = ('id', 'instalation', 'catenary', 'multitube', 'v1', 'v2')


class DischargeLocationClaimantSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    start_station = StationSerializer()
    end_station = StationSerializer()

    class Meta:
        model = DischargeLocationClaimant
        fields = ('id', 'location', 'start_station', 'end_station', 'instalation',
                  'catenary', 'multitube', 'v1', 'v2')