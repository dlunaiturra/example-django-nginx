from rest_framework import serializers
from claims.models import Claim
from profiles.serializers.user_serializer import UserSerializer
from claims.serializers.service_scope_serializer import ServiceScopeClaimantSerializer
from claims.serializers.discharge_location_serializer import DischargeLocationClaimantSerializer
from claims.serializers.discharge_type_serializer import DischargeTypeSerializer
from claims.serializers.service_type_serializer import ServiceTypeSerializer
from claims.serializers.discharge_datetime_serializer import DischargeDatetimeSerializer
from claims.serializers.maintenance_claim_serializer import MaintenanceClaimSerializer


class ClaimSerializer(serializers.ModelSerializer):
    claimant = UserSerializer()
    service = ServiceScopeClaimantSerializer()
    location = DischargeLocationClaimantSerializer()
    discharge_type = DischargeTypeSerializer()
    service_type = ServiceTypeSerializer()
    discharge_datetimes = DischargeDatetimeSerializer(many=True)
    maintenance_claim = MaintenanceClaimSerializer()

    class Meta:
        model = Claim
        fields = ('id', 'claim_num', 'claim_date', 'claimant', 'service', 'location',
                  'discharge_type', 'service_type', 'description', 'discharge_datetimes',
                  'maintenance_claim')


class SimpleClaimSerialiser(serializers.ModelSerializer):
    claimant = UserSerializer()
    maintenance_claim = MaintenanceClaimSerializer()
    service = ServiceScopeClaimantSerializer()

    class Meta:
        model = Claim
        fields = ('id', 'claim_num', 'claim_date', 'description', 'claimant', 'service', 'maintenance_claim')
