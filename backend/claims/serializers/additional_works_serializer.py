from django.contrib.auth.models import User
from rest_framework import serializers

from claims.entities.additional_work import AdditionalWork, Proposal, Price, Forecast, AssignmentHours, File
from profiles.serializers.user_serializer import SimpleUserSerializer


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('file',)


class AssignmentHoursSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignmentHours


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        exclude = ('proposal',)


class ProposalSerializer(serializers.ModelSerializer):
    maintainer = SimpleUserSerializer()
    validator_user = SimpleUserSerializer()
    prices = PriceSerializer(many=True)
    proposed_acceptance = serializers.CharField(source='get_proposed_acceptance_display')

    class Meta:
        model = Proposal
        fields = '__all__'


class ForecastSerializer(serializers.ModelSerializer):
    claimant = SimpleUserSerializer()

    class Meta:
        model = Forecast
        fields = '__all__'


class AdditionalWorksSerializer(serializers.ModelSerializer):
    proposal_main_file = FileSerializer(read_only=True)
    claimant = SimpleUserSerializer()

    class Meta:
        model = AdditionalWork
        fields = '__all__'

    def create(self, validated_data):
        claimant = validated_data.pop('claimant')
        claimant = User.objects.get_or_create(claimant)[0]
        print(claimant)
        return AdditionalWork.objects.create(claimant=claimant, **validated_data)

