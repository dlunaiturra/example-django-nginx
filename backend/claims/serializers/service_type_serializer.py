from rest_framework import serializers
from claims.models import ServiceType


class ServiceTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceType
        fields = ('id', 'type')