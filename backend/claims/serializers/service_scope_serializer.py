from rest_framework import serializers
from claims.models import ServiceScope, ServiceScopeClaimant
from claims.serializers.company_serializer import CompanySerializer


class ServiceScopeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceScope
        fields = ('id', 'operator', 'maintenance')

class ServiceScopeClaimantSerializer(serializers.ModelSerializer):
    company = CompanySerializer()

    class Meta:
        model = ServiceScopeClaimant
        fields = ('id', 'operator', 'maintenance', 'number_services', 'company')