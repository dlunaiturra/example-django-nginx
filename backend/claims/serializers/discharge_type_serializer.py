from rest_framework import serializers
from claims.models import DischargeType


class DischargeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = DischargeType
        fields = ('id', 'catenary', 'wire_25')