from rest_framework import serializers
from claims.models import Station


class StationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Station
        fields = ('id', 'name')