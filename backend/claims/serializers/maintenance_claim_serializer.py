from rest_framework import serializers
from claims.models import MaintenanceClaim
from profiles.serializers.user_serializer import UserSerializer
from claims.serializers.discharge_type_serializer import DischargeTypeSerializer
from claims.serializers.service_type_serializer import ServiceTypeSerializer
from claims.serializers.discharge_location_serializer import DischargeLocationSerializer


class MaintenanceClaimSerializer(serializers.ModelSerializer):

    user_confirm = UserSerializer()
    user_pilot = UserSerializer()
    discharge_type = DischargeTypeSerializer()
    service_type = ServiceTypeSerializer()
    location = DischargeLocationSerializer()

    class Meta:
        model = MaintenanceClaim
        fields = ('id', 'state', 'comments', 'user_confirm', 'user_pilot', 'claim',
                  'claim_date', 'discharge_type', 'service_type', 'location')