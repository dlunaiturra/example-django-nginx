from rest_framework import serializers
from claims.models import DischargeDatetime


class DischargeDatetimeSerializer(serializers.ModelSerializer):

    class Meta:
        model = DischargeDatetime
        fields = ('id', 'discharge_date', 'start_time', 'end_time')