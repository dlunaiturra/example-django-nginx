from django.contrib import admin

# Register your models here.
from claims.entities.additional_work import AdditionalWork, Proposal, Forecast, Price, AssignmentHours


@admin.register(AdditionalWork)
class AdditionalWorkAdmin(admin.ModelAdmin):
    # filter_vertical = ('proposal', )
    pass


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    pass


@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    pass


@admin.register(Forecast)
class ForecastAdmin(admin.ModelAdmin):
    pass


@admin.register(AssignmentHours)
class AssignmentHoursAdmin(admin.ModelAdmin):
    pass
