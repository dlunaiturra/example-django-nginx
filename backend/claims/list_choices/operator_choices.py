from enum import Enum


class OperatorChoices(Enum):
    CITY_HALLS = "Ayuntamientos"
    OPERATOR_WORKS = "Trabajos Operador"
    THIRD_COMPANIES = "Terceras empresas"
