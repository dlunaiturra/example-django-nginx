from enum import Enum


class MaintenanceChoices(Enum):
    ALSTOM = 'Alstom'
    COMSA = 'Comsa'
