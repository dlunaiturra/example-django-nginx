from enum import Enum


class InstalationChoices(Enum):
    TRAMBAIX = 'Trambaix'
    TRAMBESOS = 'Trambesos'
