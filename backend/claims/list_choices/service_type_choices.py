from enum import Enum


class ServiceTypeChoices(Enum):
    ENERGY = "Piloto de energía"
    WORK = "Piloto de trabajo"