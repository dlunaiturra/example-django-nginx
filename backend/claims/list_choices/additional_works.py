from enum import Enum


class CustomEnum(Enum):
    @classmethod
    def get_choices(cls):
        return [(tag.name, tag.value) for tag in cls]


class PriorityChoices(CustomEnum):
    H = 'Alta'
    M = 'Media'
    L = 'Baja'


class NetworkChoices(CustomEnum):
    TBX = 'TBX'
    TBS = 'TBS'


class ApplicationAreaChoices(CustomEnum):
    MATERIAL_RODANTE = 'Material Rodante'
    SISTEMAS = 'Sistemas'
    INFRAESTRUCTURA = 'Infraestructura'


class WorkStatusChoices(CustomEnum):
    PENDING = 'Pendiente'
    STARTED = 'Iniciado'
    HALF_FINISHED = '50%'
    FINISHED = 'Finalizado'


class StateMaintenanceChoices(CustomEnum):
    ACCEPTED = "Aceptada"
    REFUSED = "Rechazada"


class PriceGroupChoices(CustomEnum):
    MATERIALES_SUBCONTRATACION = 'Materiales/Subcontratación'
    MO = 'Mano de obra'
