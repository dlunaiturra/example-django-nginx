from enum import Enum


class StateMaintenanceChoices(Enum):
    ACCEPTED = "Aceptada"
    REFUSED = "Rechazada"