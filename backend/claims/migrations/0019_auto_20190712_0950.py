# Generated by Django 2.1.5 on 2019-07-12 07:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('claims', '0018_auto_20190711_1607'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='additionalwork',
            name='account_assignment_hours',
        ),
        migrations.RemoveField(
            model_name='additionalwork',
            name='bucket_assignment_hours',
        ),
        migrations.AddField(
            model_name='assignmenthours',
            name='account_assignment_hours',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='account_assignment_hours', to='claims.AdditionalWork', verbose_name='Imputacion horas'),
        ),
        migrations.AddField(
            model_name='assignmenthours',
            name='bucket_assignment_hours',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bucket_assignment_hours', to='claims.AdditionalWork', verbose_name='Bolsa horas'),
        ),
        migrations.AlterField(
            model_name='forecast',
            name='end_date',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='forecast',
            name='forecast_date',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='forecast',
            name='forecast_file',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='proposal',
            name='approval_date',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='proposal',
            name='delivery_date',
            field=models.DateField(blank=True),
        ),
        migrations.AlterField(
            model_name='proposal',
            name='proposed_or_priority_date',
            field=models.DateField(blank=True),
        ),
    ]
