import random
from datetime import datetime


def generate_request_number(code='STA', date=datetime.now()):
    num = random.sample(range(1, 99999), 1)[0]
    return '{}-{}-{}'.format(code, int(date.year), num)

