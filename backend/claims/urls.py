from django.conf.urls import url

from claims.api.additional_works_views import AdditionalWorksListView, AdditionalWorksUploadFiles
from claims.api.company_views import CompanyListView
from claims.api.location_views import LocationListView
from claims.api.claim_views import ClaimView, ClaimListView
from claims.api.station_views import StationListView
from django.urls import path, re_path, include

urlpatterns = [
    path('companies/', CompanyListView.as_view()),
    path('locations/', LocationListView.as_view()),
    path('stations/', StationListView.as_view()),
    path('claim/', ClaimView.as_view()),
    path('works/', AdditionalWorksListView.as_view()),
    path('works/upload/<int:pk>/', AdditionalWorksUploadFiles.as_view()),
    re_path(r'^claim/(?P<pk>\d+)$', ClaimView.as_view()),
    path('claims/', ClaimListView.as_view()),
    url('', StationListView)
]
