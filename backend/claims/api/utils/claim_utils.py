from django.contrib.auth.models import User
from claims.models import Company, DischargeLocationClaimant, Location, Station, DischargeLocation, \
    DischargeType, ServiceType, DischargeDatetime, Claim, MaintenanceClaim, ServiceScope, ServiceScopeClaimant
from datetime import date


class FieldManager(object):

    model = None
    fields_update = '__all__'
    fields_create = '__all__'
    fields_type_date = []

    def convert_to_date(self, date_str):
        dt = None
        if date_str:
            dt_str = date_str.split(' ')[0]
            calendar = dt_str.split('-')
            dt = date(int(calendar[0]), int(calendar[1]), int(calendar[2]))
        return dt

    def update(self, info):
        obj = self.__class__.model.objects.get(pk = info['id'])
        for (key, value) in info.items():
            if self.__class__.fields_update == '__all__' or key in self.__class__.fields_update:
                setattr(obj, key, value)
        return obj

    def get_params_create(self, info):
        params = {}
        for (key, value) in info.items():
            if self.__class__.fields_create == '__all__' or key in self.__class__.fields_create:
                if key in self.__class__.fields_type_date:
                    params[key] = self.convert_to_date(value)
                else:
                    params[key] = value
        return params

    def create(self, info):
        params = self.get_params_create(info)
        obj = self.__class__.model(**params)
        return obj

    def get_field(self, info):
        if 'id' in info:
            obj = self.update(info)
        else:
            obj = self.create(info)
        obj.save()
        return obj


class UserManager(FieldManager):

    model = User
    fields_update = ['email']
    fields_type_date = []

    def update_profile(self, claimant, info):
        if 'phone' in info['profile']:
            claimant.profile.phone = info['profile']['phone']
        if 'mobile' in info['profile']:
            claimant.profile.mobile = info['profile']['mobile']
        claimant.profile.save()

    def update(self, info):
        claimant = super(UserManager, self).update(info)
        self.update_profile(claimant, info)
        return claimant

    def get_params_create(self, info):
        names = info['complete_name'].lstrip().split(' ')
        params = {
            'first_name': names[0],
            'last_name': ' '.join(names[1:]),
            'email': info['email'],
            'username': info['complete_name'],
        }
        return params

    def create(self, info):
        params = self.get_params_create(info)
        claimant = User(**params)
        claimant.save()
        self.update_profile(claimant, info)
        return claimant


class CompanyManager(FieldManager):

    model = Company


class ServiceClaimantManager(FieldManager):

    model = ServiceScopeClaimant
    fields_update = ['operator', 'number_services', 'maintenance']
    fields_create = ['operator', 'number_services', 'maintenance']

    def __init__(self):
        self.company_manager = CompanyManager()

    def update(self, info):
        service = super(ServiceClaimantManager, self).update(info)
        service.company = self.company_manager.get_field(info['company'])
        return service

    def create(self, info):
        service = super(ServiceClaimantManager, self).create(info)
        service.company = self.company_manager.get_field(info['company'])
        return service


class ServiceManager(FieldManager):

    model = ServiceScope
    fields_update = ['operator', 'maintenance']
    fields_create = ['operator', 'maintenance']


class LocationManager(FieldManager):

    model = Location


class StationManager(FieldManager):

    model = Station


class DischargeLocationManager(FieldManager):

    model = DischargeLocation
    fields_update = ['instalation', 'catenary', 'multitube', 'v1', 'v2']
    fields_create = ['instalation', 'catenary', 'multitube', 'v1', 'v2']


class DischargeLocationClaimantManager(FieldManager):

    model = DischargeLocationClaimant
    fields_update =  ['instalation', 'catenary', 'multitube', 'v1', 'v2']
    fields_create = ['instalation', 'catenary', 'multitube', 'v1', 'v2']

    def __init__(self):
        self.location_manager = LocationManager()
        self.station_manager = StationManager()

    def update(self, info):
        location = super(DischargeLocationClaimantManager, self).update(info)
        location.location = self.location_manager.get_field(info['location'])
        location.start_station = self.station_manager.get_field(info['start_station'])
        location.end_station = self.station_manager.get_field(info['end_station'])
        return location

    def create(self, info):
        location = super(DischargeLocationClaimantManager, self).create(info)
        location.location = self.location_manager.get_field(info['location'])
        location.start_station = self.station_manager.get_field(info['start_station'])
        location.end_station = self.station_manager.get_field(info['end_station'])
        return location


class DischargeTypeManager(FieldManager):

    model = DischargeType


class ServiceTypeManager(FieldManager):

    model = ServiceType


class DischargeDatetimeManager(FieldManager):

    model = DischargeDatetime


class MaintenanceClaimManager(FieldManager):

    model = MaintenanceClaim
    fields_create = ['claim']
    fields_update = ['comments', 'state', 'claim_date']

    def relationals_fields(self, claim, info):
        claim.service = ServiceManager().get_field(info['service'])
        claim.location = DischargeLocationManager().get_field(info['location'])
        claim.discharge_type = DischargeTypeManager().get_field(info['discharge_type'])
        claim.service_type = ServiceTypeManager().get_field(info['service_type'])

    def update(self, info):
        claim = super(MaintenanceClaimManager, self).update(info)
        self.relationals_fields(claim, info)
        claim.user_pilot = UserManager().get_field(info['user_pilot'])
        claim.user_confirm = UserManager().get_field(info['user_confirm'])
        return claim

    def create(self, info):
        claim = super(MaintenanceClaimManager, self).create(info)
        self.relationals_fields(claim, info)
        return claim


class ClaimManager(FieldManager):

    model = Claim
    fields_update = ['description']
    fields_create = ['claim_num', 'claim_date', 'description']
    fields_type_date = ['claim_date']

    def create_maintenance(self, info, claim):
        params = {
            'claim': claim,
            'discharge_type': info['discharge_type'],
            'service_type': info['service_type'],
            'service': info['service'],
            'location': info['location']
        }
        maintenance = MaintenanceClaimManager().get_field(params)
        claim.maintenance_claim = maintenance

    def relationals_fields(self, claim, info):
        claim.claimant = UserManager().get_field(info['claimant'])
        claim.service = ServiceClaimantManager().get_field(info['service'])
        claim.location = DischargeLocationClaimantManager().get_field(info['location'])
        claim.discharge_type = DischargeTypeManager().get_field(info['discharge_type'])
        claim.service_type = ServiceTypeManager().get_field(info['service_type'])
        if 'maintenance_claims' in info:
            MaintenanceClaimManager().get_field(info['maintenance_claims'])

    def update(self, info):
        claim = super(ClaimManager, self).update(info)
        self.relationals_fields(claim, info)
        return claim

    def create(self, info):
        params = self.get_params_create(info)
        claim = Claim(**params)
        self.relationals_fields(claim, info)
        claim.save()
        self.create_maintenance(info, claim)
        return claim


class ListManager(object):

    manager = None
    relational_field = ''

    def create_list(self, list, obj_relational):
        for row in list:
            row[self.__class__.relational_field] = obj_relational
            self.__class__.manager.get_field(row)

    def ids_exists(self, list):
        ids = []
        for row in list:
            if 'id' in row and row['id']:
                ids.append(row['id'])
        return ids

    def update_list(self, list, obj_relational):
        model = getattr(self.__class__.manager, 'model')
        params = {
            self.__class__.relational_field: obj_relational
        }
        query = model.objects.filter(**params)
        ids = self.ids_exists(list)
        if len(ids) > 0:
            query = query.exclude(id__in=ids)
        query.delete()
        self.create_list(list, obj_relational)


class DatetimesListManager(ListManager):

    manager = DischargeDatetimeManager()
    relational_field = 'claim'
