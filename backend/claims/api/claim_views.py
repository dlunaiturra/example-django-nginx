from rest_framework import generics
from claims.serializers.claim_serializer import ClaimSerializer, SimpleClaimSerialiser
from claims.models import Claim
from rest_framework import status
from rest_framework.response import Response
from django.db import transaction
from claims.api.utils.claim_utils import ClaimManager, DatetimesListManager
from rest_framework_tracking.mixins import LoggingMixin


class ClaimListView(LoggingMixin, generics.ListAPIView):
    queryset = Claim.objects.all()
    serializer_class = SimpleClaimSerialiser
    authentication_classes = ()
    permission_classes = ()

    def get_queryset(self):
        queryset = super(ClaimListView, self).get_queryset()
        if 'claim_num' in self.request.GET:
            claim_num = self.request.GET['claim_num']
            queryset = queryset.filter(claim_num__icontains=claim_num)
        return queryset


class ClaimView(LoggingMixin, generics.CreateAPIView, generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = ClaimSerializer
    authentication_classes = ()
    permission_classes = ()
    queryset = Claim.objects.all()

    def update(self, request, *args, **kwargs):
        result = {}
        try:
            with transaction.atomic():
                claim = ClaimManager().get_field(request.data)
                DatetimesListManager().update_list(request.data['discharge_datetimes'], claim)
            result = self.__class__.serializer_class(claim).data
            headers = self.get_success_headers(result)
            response = Response(result, status=status.HTTP_200_OK, headers=headers)
        except Exception as e:
            import traceback
            traceback.print_exc()
            result['error'] = str(e)
            headers = self.get_success_headers(result)
            response = Response(result, status=status.HTTP_400_BAD_REQUEST, headers=headers)
        return response

    def create(self, request, *args, **kwargs):
        result = {}
        try:
            with transaction.atomic():
                claim = ClaimManager().get_field(request.data)
                DatetimesListManager().create_list(request.data['discharge_datetimes'], claim)
            result = self.__class__.serializer_class(claim).data
            headers = self.get_success_headers(result)
            response = Response(result, status=status.HTTP_201_CREATED, headers=headers)
        except Exception as e:
            import traceback
            traceback.print_exc()
            result['error'] = str(e)
            headers = self.get_success_headers(result)
            response = Response(result, status=status.HTTP_400_BAD_REQUEST, headers=headers)
        return response
