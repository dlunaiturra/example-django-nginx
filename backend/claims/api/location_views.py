from rest_framework import generics
from claims.models import Location
from claims.serializers.location_serializer import LocationSerializer


class LocationListView(generics.ListAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    pagination_class = None