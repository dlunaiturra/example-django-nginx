from rest_framework import generics, status, views
from rest_framework.parsers import FormParser, FileUploadParser
from rest_framework.response import Response
from rest_framework_tracking.mixins import LoggingMixin

from claims.entities.additional_work import AdditionalWork, File
from claims.serializers.additional_works_serializer import AdditionalWorksSerializer, FileSerializer


class AdditionalWorksListView(LoggingMixin, generics.ListAPIView, generics.CreateAPIView):
    serializer_class = AdditionalWorksSerializer
    queryset = AdditionalWork.objects.all()
    authentication_classes = ()
    permission_classes = ()
    pagination_class = None

    # def get_queryset(self):
    #     queryset = super(AdditionalWorksListView, self).get_queryset()
    #     if 'pk' in self.request.GET:
    #         pk = self.request.GET['pk']
    #         queryset = queryset.filter(claim_num__icontains=pk)
    #     return queryset

    def create(self, request, *args, **kwargs):

        validator = self.serializer_class(data=request.data)

        if validator.is_valid():
            validator.save()
            return Response(validator.data, status=status.HTTP_201_CREATED)
        return Response(validator.errors, status=status.HTTP_406_NOT_ACCEPTABLE)


class AdditionalWorksUploadFiles(views.APIView):
    parser_classes = (FileUploadParser, FormParser)
    queryset = File.objects.all()
    serializer_class = FileSerializer
    authentication_classes = ()
    permission_classes = ()

    def put(self, request, pk):
        validator = self.serializer_class(data=request.data)

        if validator.is_valid():
            validator.save()
            return Response(validator.data)
