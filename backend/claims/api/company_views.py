from rest_framework import generics
from claims.models import Company
from claims.serializers.company_serializer import CompanySerializer


class CompanyListView(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    pagination_class = None
