from rest_framework import generics
from claims.models import Station
from claims.serializers.station_serializer import StationSerializer


class StationListView(generics.ListAPIView):
    queryset = Station.objects.all()
    serializer_class = StationSerializer
    pagination_class = None