from django.db import models
from claims.list_choices.operator_choices import OperatorChoices
from claims.list_choices.maintenance_choices import MaintenanceChoices


class ServiceScope(models.Model):
    OPERATOR_CHOICES = [(tag.name, tag.value) for tag in OperatorChoices]
    MAINTENANCE_CHOICES = [(tag.name, tag.value) for tag in MaintenanceChoices]

    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    operator = models.CharField(
        max_length=100,
        choices=OPERATOR_CHOICES,
        verbose_name="Operador",
        default=OperatorChoices.THIRD_COMPANIES.name
    )
    maintenance = models.CharField(
        max_length=100,
        choices=MAINTENANCE_CHOICES,
        verbose_name="Consorcio Mantenimiento",
        blank=True,
        null=True
    )


class ServiceScopeClaimant(ServiceScope):

    number_services = models.IntegerField(
        verbose_name="Número Servicios de Piloto Solicitados"
    )
    company = models.ForeignKey("claims.Company",
                                on_delete=models.CASCADE,
                                related_name='services',
                                verbose_name='Empresa')