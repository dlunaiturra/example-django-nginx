from django.db import models
from django.conf import settings


class Claim(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    claim_num = models.CharField(max_length=30,
                                 verbose_name="Num. Solicitud")
    claim_date = models.DateField(verbose_name="Fecha Solicitud", auto_now_add=True)
    claimant = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='claims',
                                 verbose_name='Solicitante'
                                 )
    service = models.OneToOneField("claims.ServiceScopeClaimant",
                                   on_delete=models.CASCADE,
                                   related_name='claim',
                                   verbose_name='Ámbito del Servicio de Piloto'
                                   )
    location = models.OneToOneField("claims.DischargeLocationClaimant",
                                    on_delete=models.CASCADE,
                                    related_name='claim',
                                    verbose_name='Datos Ubicación Descargo'
                                    )
    discharge_type = models.OneToOneField("claims.DischargeType",
                                          on_delete=models.CASCADE,
                                          related_name="claim",
                                          verbose_name="Tipo de Descargo")
    service_type = models.OneToOneField("claims.ServiceType",
                                        on_delete=models.CASCADE,
                                        related_name="claim",
                                        verbose_name="Tipo de Servicio de Piloto")
    description = models.TextField(null=True, blank=True)
