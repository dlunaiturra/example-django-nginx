from django.db import models
from claims.list_choices.service_type_choices import ServiceTypeChoices


class ServiceType(models.Model):
    TYPE_CHOICES = [(tag.name, tag.value) for tag in ServiceTypeChoices]

    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    type = models.CharField(
        max_length=100,
        choices=TYPE_CHOICES,
        verbose_name="Tipo Piloto",
    )
