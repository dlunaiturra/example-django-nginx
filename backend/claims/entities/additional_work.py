from django.db import models
from django.conf import settings

from claims.list_choices.additional_works import PriorityChoices, NetworkChoices, ApplicationAreaChoices, \
    StateMaintenanceChoices, WorkStatusChoices, PriceGroupChoices


class File(models.Model):
    file = models.FileField(upload_to='additional-works', blank=True)

    def __str__(self):
        return self.file.name


class Proposal(models.Model):
    """

    """
    name = models.CharField(max_length=100, blank=True, default='')
    proposal_str_id = models.CharField(max_length=200, blank=True)
    proposed_acceptance = models.CharField(max_length=50, choices=StateMaintenanceChoices.get_choices(),
                                           default='ACCEPTED')
    comments = models.CharField(max_length=200, blank=True, null=True)
    proposal_file = models.FileField(upload_to='', default='File')
    work_planning_file = models.FileField(upload_to='', default='File')
    internal_documentation_file = models.FileField(upload_to='', default='File')
    offer_request_file = models.FileField(upload_to='', default='File')
    order_file = models.FileField(upload_to='', default='File')
    delivery_date = models.DateField(blank=True)
    approval_date = models.DateField(blank=True)
    proposed_or_priority_date = models.DateField(blank=True)
    maintainer = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.CASCADE,
                                   related_name='proposal_maintainer',
                                   verbose_name='Mantenedor')
    validator_user = models.ForeignKey(settings.AUTH_USER_MODEL,
                                       on_delete=models.CASCADE,
                                       related_name='proposal_validator',
                                       verbose_name='Validador')

    def __str__(self):
        return self.name


class Price(models.Model):
    """

    """
    parent_group = models.CharField(max_length=100, choices=PriceGroupChoices.get_choices(), default='MO')
    concept = models.CharField(max_length=200, default=None)
    duration = models.FloatField(default=0, blank=True)
    price = models.FloatField(default=0, blank=True)
    proposal = models.ForeignKey(Proposal, null=True,
                                 on_delete=models.CASCADE,
                                 related_name='prices',
                                 verbose_name='Precio propuesta')

    def __str__(self):
        return self.concept


class Forecast(models.Model):
    """

    """
    name = models.CharField(max_length=100)
    claimant = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='forecast',
                                 verbose_name='Solicitante')
    forecast_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    forecast_file = models.FileField(upload_to='')
    status = models.CharField(max_length=50, choices=WorkStatusChoices.get_choices(), default='Pendiente')

    comments = models.CharField(max_length=100, default=None)

    def __str__(self):
        return self.name


class AdditionalWork(models.Model):
    """
        Represents additional works form
    """
    name = models.CharField(max_length=255)
    request_num = models.CharField(max_length=255)
    request_date = models.DateField(auto_now_add=True)
    description = models.TextField(max_length=250, null=True)
    proposal_main_file = models.ForeignKey(File, null=True,
                                           on_delete=models.CASCADE,
                                           related_name='additional_works',
                                           verbose_name='Fichero Propuesta')
    claimant = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='additional_works',
                                 verbose_name='Solicitante')

    priority = models.CharField(max_length=10, choices=PriorityChoices.get_choices(), default='MEDIA')
    network = models.CharField(max_length=100, choices=NetworkChoices.get_choices(), default='TBX')
    application_area = models.CharField(max_length=50, choices=ApplicationAreaChoices.get_choices(), default='SISTEMAS')

    proposal = models.OneToOneField(Proposal, null=True,
                                    on_delete=models.CASCADE, related_name='additional_works', verbose_name='Propuesta')
    forecast = models.OneToOneField(Forecast, on_delete=models.CASCADE,
                                    null=True, related_name='additional_works', verbose_name='Forecast')

    def __str__(self):
        return self.name


class AssignmentHours(models.Model):
    """

    """
    name = models.CharField(max_length=200, blank=True)
    material_rodante = models.FloatField(default=0, blank=True)
    infra = models.FloatField(default=0, blank=True)

    account_assignment_hours = models.ForeignKey(AdditionalWork, null=True,
                                                 on_delete=models.CASCADE,
                                                 related_name='account_assignment_hours',
                                                 verbose_name='Imputacion horas')
    bucket_assignment_hours = models.ForeignKey(AdditionalWork, null=True,
                                                on_delete=models.CASCADE,
                                                related_name='bucket_assignment_hours',
                                                verbose_name='Bolsa horas')

    def __str__(self):
        return self.name
