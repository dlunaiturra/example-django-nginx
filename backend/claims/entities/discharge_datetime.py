from django.db import models


class DischargeDatetime(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    discharge_date = models.DateField(verbose_name="Fecha Descargo")
    start_time = models.TimeField(verbose_name="Hora Inicio")
    end_time = models.TimeField(verbose_name="Hora de fin")
    claim = models.ForeignKey("claims.Claim",
                              on_delete=models.CASCADE,
                              related_name='discharge_datetimes',
                              )