from django.db import models


class DischargeType(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    catenary = models.BooleanField(verbose_name="Catenaria y Feeder")
    wire_25 = models.BooleanField(verbose_name="Cable 25KV")