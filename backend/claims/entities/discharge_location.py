from django.db import models
from claims.list_choices.instalation_choices import InstalationChoices


class DischargeLocation(models.Model):
    INSTALATION_CHOICES = [(tag.name, tag.value) for tag in InstalationChoices]
    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    instalation = models.CharField(
        max_length=100,
        choices=INSTALATION_CHOICES,
        verbose_name="Instalación",
    )
    catenary = models.BooleanField(verbose_name="Catenaria")
    multitube = models.BooleanField(verbose_name="Multitubular")
    v1 = models.BooleanField(verbose_name="V1")
    v2 = models.BooleanField(verbose_name="V2")


class DischargeLocationClaimant(DischargeLocation):
    location = models.ForeignKey("claims.Location",
                                 on_delete=models.CASCADE,
                                 related_name='dicharge_locations',
                                 verbose_name='Población'
                                 )
    start_station = models.ForeignKey("claims.Station",
                                      on_delete=models.CASCADE,
                                      verbose_name="Parada o Poste Inicial",
                                      related_name="start_locations")
    end_station = models.ForeignKey("claims.Station",
                                    on_delete=models.CASCADE,
                                    verbose_name="Parada o Poste Final",
                                    related_name="end_locations")