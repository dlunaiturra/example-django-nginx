from django.db import models
from django.conf import settings
from claims.list_choices.state_maintenance_choices import StateMaintenanceChoices


class MaintenanceClaim(models.Model):
    STATE_CHOICES = [(tag.name, tag.value) for tag in StateMaintenanceChoices]

    create_date = models.DateTimeField(auto_now_add=True)
    write_date = models.DateTimeField(auto_now=True)
    state = models.CharField(
        max_length=100,
        choices=STATE_CHOICES,
        verbose_name="Estado",
        null=True,
        blank=True
    )
    user_confirm = models.ForeignKey(settings.AUTH_USER_MODEL,
                                     on_delete=models.CASCADE,
                                     verbose_name='Datos Confirmación',
                                     related_name='maintenance_claims_confirm',
                                     null=True,
                                     blank=True)
    user_pilot = models.ForeignKey(settings.AUTH_USER_MODEL,
                                   on_delete=models.CASCADE,
                                   verbose_name='Datos Piloto',
                                   related_name='maintenance_claims_polit',
                                   null=True,
                                   blank=True)
    claim = models.OneToOneField(
        'claims.Claim',
        on_delete=models.CASCADE,
        related_name='maintenance_claim',
        verbose_name='Solicitud'
    )
    claim_date = models.DateField(verbose_name="Fecha Solicitud",
                                  null=True,
                                  blank=True)
    service = models.OneToOneField("claims.ServiceScope",
                                on_delete=models.CASCADE,
                                related_name='maintenance_claim',
                                verbose_name='Ámbito del Servicio de Piloto'
                                )
    location = models.OneToOneField('claims.DischargeLocation',
                                 on_delete=models.CASCADE,
                                 related_name='maintenance_claim',
                                 verbose_name='Datos Ubicación Descargo')
    discharge_type = models.OneToOneField("claims.DischargeType",
                                       on_delete=models.CASCADE,
                                       related_name="maintenance_claim",
                                       verbose_name="Tipo de Descargo")
    service_type = models.OneToOneField("claims.ServiceType",
                                     on_delete=models.CASCADE,
                                     related_name="maintenance_claim",
                                     verbose_name="Tipo de Servicio de Piloto")
    comments = models.TextField(null=True, blank=True)

