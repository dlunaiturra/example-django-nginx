#!/bin/bash
cd /frontend/tram-manager
npm install
npm rebuild node-sass
ng serve --host 0.0.0.0 --port 4200 --public-host=localhost:4200/
