import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxSpinnerModule
  ],
  declarations: [
    LoginComponent
  ],
})

export class LoginModule {}
