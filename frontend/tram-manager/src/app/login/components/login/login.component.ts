import {Component, OnInit} from '@angular/core';
import {ConnectionService} from '../../../services/connection.service';
import {ProfileService} from '../../../services/profile.service';
import {Router} from '@angular/router';
import {urls} from '../../../urls';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    public username: string;
    public password: string;
    public error: any;

    constructor(private router: Router,
                private connection: ConnectionService,
                public profile: ProfileService) {
        this.init();
    }

    private init = () => {
        this.error = {
            show: false,
            message: ''
        };
    }

    public login = () => {
        const params = {username: this.username, password: this.password};
        this.init();
        this.connection.post(this.connection.apis.login, params).subscribe(
            response => {
                this.init();
                this.connection.setCookie('csrftoken', response.token);
                this.router.navigate([urls.claims_list]);
            },
            error => {
                this.error.show = true;
                this.error.message = error.statusText;
                if (error.error) {
                    this.error.message = error.error.error;
                }

            }
        );

    }
}
