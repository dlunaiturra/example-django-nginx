import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginModule} from "./login/login.module";
import {ConnectionService} from "./services/connection.service";
import {ProfileService} from "./services/profile.service";
import {HttpClientModule} from "@angular/common/http";
import {CookieService} from "angular2-cookie/core";
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {ClaimsModule} from "./claims/claims.module";
import {MenuService} from "./menu/components/menu.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClaimEditService} from "./services/claim.edit.service";
import {AdditionalWorksComponent} from "./claims/components/additional-works/additional-works.component";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        AppRoutingModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        AngularFontAwesomeModule,
        AppRoutingModule,
        LoginModule,
        ClaimsModule,
        BrowserAnimationsModule
    ],
    providers: [
        ConnectionService,
        ProfileService,
        CookieService,
        MenuService,
        ClaimEditService,
        AdditionalWorksComponent
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
