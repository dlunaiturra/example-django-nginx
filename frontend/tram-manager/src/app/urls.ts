export const urls = {
  basic: '',
  login: 'login',
  claims_list: 'claims',
  claim: 'claim',
  works: 'add-works',
  work: 'work',
  additionalWorks: 'additionalWorks'
};
