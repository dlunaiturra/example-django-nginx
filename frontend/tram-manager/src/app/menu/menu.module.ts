import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {NgxSpinnerModule} from "ngx-spinner";
import {MenuComponent} from "./components/menu.component";
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule, MatButtonModule} from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxSpinnerModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule
    ],
    declarations: [
        MenuComponent

    ]
})
export class MenuModule {

}
