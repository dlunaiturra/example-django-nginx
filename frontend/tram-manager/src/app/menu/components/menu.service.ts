import {Injectable} from '@angular/core';
import {urls} from '../../urls';
import {MenuModel} from '../../models/menu/menu.model';
import {UserModel} from '../../models/user.model';
import {ProfileService} from "../../services";


@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private treeMenu = [
        {
            name: 'Solicitudes',
            id: 'Id1',
            children: [
                {
                    name: 'Listado Solicitudes',
                    url: urls.claims_list
                },
                {
                    name: 'Crear Solicitud',
                    url: urls.claim
                },
                {
                    name: 'Trabajos adicionales',
                    url: urls.additionalWorks
                }
            ]
        }
    ];

    public typeSearcher: boolean;
    public typeEdit: boolean;
    public save: Function;
    public urls: object;
    public menu: MenuModel;
    public user: UserModel;

    constructor(private profile: ProfileService) {
        this.urls = urls;
        this.menu = new MenuModel(this.treeMenu);
        this.user = profile.getUserProfileStoredData()
    }

    public activeSearcher = () => {
        this.typeSearcher = true;
        this.typeEdit = false;
    }

    public activeEdit = () => {
        this.typeSearcher = false;
        this.typeEdit = true;
    }
}
