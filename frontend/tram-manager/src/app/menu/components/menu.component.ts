import {Component} from '@angular/core';
import {MenuService} from './menu.service';
import {ItemMenuModel} from '../../models/menu/item.menu.model';
import {Router} from '@angular/router';
import {SearcherService} from '../../services/searcher.service';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
    [x: string]: any;

    constructor(public serviceMenu: MenuService,
                public searcher: SearcherService,
                private router: Router,
                private authService: AuthService) {
    }

    public goTo = (item: ItemMenuModel, active: ItemMenuModel) => {
        this.router.navigate([item.url]);
    }


    public logout() {
        this.authService.logout();
        this.router.navigate(['/login']);
    }
}
