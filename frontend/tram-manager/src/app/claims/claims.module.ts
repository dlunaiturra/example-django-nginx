import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClaimSearcherComponent} from './components/claim-searcher/claim-searcher.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxSpinnerModule} from 'ngx-spinner';
import {MenuComponent} from '../menu/components/menu.component';
import {ClaimEditComponent} from './components/claim-edit/claim-edit.component';
import {BrowserModule} from '@angular/platform-browser';
import {
    MatAutocompleteModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule
} from '@angular/material';
import {AdditionalWorksComponent} from './components/additional-works/additional-works.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxSpinnerModule,
        BrowserModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatIconModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
    ],
    declarations: [
        ClaimSearcherComponent,
        MenuComponent,
        ClaimEditComponent,
        AdditionalWorksComponent,
    ],
    exports: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ClaimsModule {
}
