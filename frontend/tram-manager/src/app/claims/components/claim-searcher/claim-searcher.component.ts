import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MenuService} from '../../../menu/components/menu.service';
import {ConnectionService, SearcherService} from '../../../services';
import {ClaimModel} from '../../../models/claim.model';
import {urls} from '../../../urls';


@Component({
    selector: 'app-claim-searcher',
    templateUrl: './claim-searcher.component.html',
    styleUrls: ['./claim-searcher.component.scss'],
})
export class ClaimSearcherComponent implements OnInit {

    public list: ClaimModel [];

    constructor(public serviceMenu: MenuService,
                public searcher: SearcherService,
                private router: Router,
                public connection: ConnectionService) {
    }

    ngOnInit() {
        this.serviceMenu.activeSearcher();
        this.searcher.search = this.search;
        this.searcher.next = this.next;
        this.searcher.previous = this.previous;
        this.getData();
    };

    private resetData = (data) => {
        this.list = [];
        for (const claim of data) {
            console.log(new ClaimModel(claim));
            this.list.push(new ClaimModel(claim));
        }
    };

    private prepareParams = () => {
        let params = {};
        if (this.searcher.filter.name) {
            params['claim_num'] = this.searcher.filter.name;
        }
        if (this.searcher.filter['limit'] !== undefined) {
            params['limit'] = this.searcher.filter['limit'];
        }
        if (this.searcher.filter['offset'] !== undefined) {
            params['offset'] = this.searcher.filter['offset'];
        }
        return params;
    };

    private getData = () => {
        let params = this.prepareParams();
        this.connection.get(this.connection.apis.claims_list, params, true).subscribe(
            response => {
                const result = response['results'];
                this.searcher.update(response['count'], response['next'], response['previous']);
                this.resetData(result);
            },
            err => console.log(err)
        )
    };

    public goToDetail = (item: ClaimModel) => {
        this.router.navigate([urls.claim, item.id]);
    };

    public search = () => {
        this.searcher.initPagination();
        this.getData();
    };

    public next = () => {
        this.searcher.filter['offset'] = this.searcher.nextOffset;
        this.getData();
    };

    public previous = () => {
        this.searcher.filter['offset'] = this.searcher.previousOffset;
        this.getData();
    };

}
