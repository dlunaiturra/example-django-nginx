import {Component, OnInit} from '@angular/core';
import {ClaimModel} from '../../../models/claim.model';
import {MenuService} from '../../../menu/components/menu.service';
import {ConnectionService} from '../../../services/connection.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {urls} from '../../../urls';
import {ClaimEditService} from '../../../services/claim.edit.service';
import {GeneralDataService} from '../../../services/general.data.service';


@Component({
    selector: 'app-claim-edit',
    templateUrl: './claim-edit.component.html',
    styleUrls: ['./claim-edit.component.scss']
})
export class ClaimEditComponent implements OnInit {

    public claim: ClaimModel;

    constructor(public serviceMenu: MenuService,
                public serviceClaim: ClaimEditService,
                public dataService: GeneralDataService,
                private route: ActivatedRoute,
                private connection: ConnectionService,
                private location: Location) {
    }

    ngOnInit() {
        this.newData();
        this.dataService.fetchData();
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getData(parseInt(id));
            this.serviceMenu.save = this.save;
        } else {
            this.serviceMenu.save = this.create;
        }
        this.serviceMenu.activeEdit();
    }

    private newData = () => {
        this.claim = new ClaimModel();
        this.serviceClaim.setClaim(this.claim);
    }

    private getData = (id: number) => {
        const params = {};
        const url = this.connection.apis.claim + id;
        this.connection.get(url, params, true).subscribe(
            response => {
                this.claim = new ClaimModel(response);
                this.serviceClaim.setClaim(this.claim);
            },
            error => {
            }
        );
    }

    public save = () => {
        const url = this.connection.apis.claim + this.claim.id;
        this.connection.put(url, this.claim, true).subscribe(
            response => {
                this.claim = new ClaimModel(response);
                this.serviceClaim.setClaim(this.claim);
            },
            error => {
            }
        );
    }

    public create = () => {
        this.connection.post(this.connection.apis.claim, this.claim, true).subscribe(
            response => {
                this.claim = new ClaimModel(response);
                this.serviceClaim.setClaim(this.claim);
                this.serviceMenu.save = this.save;
                const url = urls.claim + '/' + this.claim.id;
                this.location.go(url);
            },
            error => {
            }
        );
    }


}
