import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalWorksComponent } from './additional-works.component';

describe('AdditionalWorksComponent', () => {
  let component: AdditionalWorksComponent;
  let fixture: ComponentFixture<AdditionalWorksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalWorksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalWorksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
