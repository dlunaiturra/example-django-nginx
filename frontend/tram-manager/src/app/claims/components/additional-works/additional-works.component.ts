import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {AdditionalWorksModel} from '../../../models/additional.works.model';
import {MenuService} from '../../../menu/components/menu.service';
import {AdditionalWorksService, GeneralDataService, ConnectionService} from '../../../services';
import {urls} from '../../../urls';
import {ClaimModel} from "../../../models/claim.model";

@Component({
    selector: 'app-additional-works',
    templateUrl: './additional-works.component.html',
    styleUrls: ['./additional-works.component.scss']
})
export class AdditionalWorksComponent implements OnInit {

    public works: AdditionalWorksModel;
    private formData: FormData = new FormData();
    public files: Map<string, File> = new Map();

    constructor(public serviceMenu: MenuService,
                public serviceWorks: AdditionalWorksService,
                public dataService: GeneralDataService,
                private route: ActivatedRoute,
                private connection: ConnectionService,
                private location: Location) {
    }

    ngOnInit() {
        this.newWorks();
        this.checkMenuUsage();
        this.serviceMenu.activeEdit();
    }

    private newWorks = () => {
        this.works = new AdditionalWorksModel();
        this.serviceWorks.setAdditionalWorks(this.works);
    }

    private checkMenuUsage = () => {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.getData(parseInt(id));
            this.serviceMenu.save = this.save;
        } else {
            this.serviceMenu.save = this.create;
        }
    }


    // TODO: Modificar para Additional Works
    private getData = (id: number) => {
        const url = this.connection.apis.additional_works + id;
        this.connection.get(urls.works.concat(`/${id}`), {}, true).subscribe(
            res => console.log(res),
            // this.claim = new ClaimModel(response);
            // this.serviceWorks.setAdditionalWorks(this.claim);
            err => console.log(err)
        );
    }


    // TODO: Modificar para Additional Works
    public save = () => {
        // const url = this.connection.apis + this.works.id;
        this.connection.put(urls.works, this.works, true).subscribe(
            res => console.log(res)
            // this.works = new ClaimModel(response);
            // this.serviceClaim.setClaim(this.works);
            ,
            err => console.log(err)
        );
    }

    public create = () => {

        this.works.proposal = null;
        this.works.forecast = null;

        this.connection.post(this.connection.apis.additional_works, this.works, true).subscribe(
            res => {
                this.works = new AdditionalWorksModel(res);
                this.serviceWorks.setAdditionalWorks(this.works);
                this.serviceMenu.save = this.save;
                const url = urls.work + '/' + this.works.id;
                this.location.go(url);
            },
            err => {
                console.log(err);
            }
        );
    }

    private uploadFiles(): void {
        if (this.files.size > 0) {
            this.files.forEach((value, index) => {
                    this.formData.append(index, value);
                }
            )
        }
        this.connection.put(`claims/${urls.additionalWorks}/upload/1/`, this.formData, true).subscribe(
            res => console.log(res)
            // this.claim = new ClaimModel(response);
            // this.serviceClaim.setClaim(this.claim);
            // this.serviceMenu.save = this.save;
            // const url = urls.claim + '/' + this.claim.id;
            // this.location.go(url);
            ,
            err => console.log(err)
        );
    }

    private onFileChange(event) {
        let file = event.target.files[0];
        if (event.target.files.length > 0) {
            let fileName = event.target.name;
            this.files.set(fileName, file);
        }

        console.log(this.files);
    }
}
