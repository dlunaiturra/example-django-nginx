export class DischargeTypeModel {

    public id: number;
    public catenary: boolean;
    public wire_25: boolean;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.catenary = data['catenary'];
        this.wire_25 = data['wire_25'];
    };

    public initDefaultValues = () => {
        this.catenary = false;
        this.wire_25 = false;
    };
}
