export class ProfileModel {

    public id: number;
    public phone: string;
    public mobile: string;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.phone = data['phone'];
        this.mobile = data['mobile'];
    };
}
