import {UserModel} from './user.model';
import {ModelInterface} from './model.interface';

export class ForecastModel implements ModelInterface {
    public claimant: UserModel;
    public forecast_date: string;
    public forecast_file: string;
    public status: string;
    public end_date: string;
    public comments: string;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    }

    initDefaultValues() {
        this.claimant = new UserModel();
        this.forecast_date = this.getCurrentDate();
    }

    initValues(data?: object) {
    }

    getCurrentDate(): any {
        return new Date().toISOString().split('T')[0];
    }

}
