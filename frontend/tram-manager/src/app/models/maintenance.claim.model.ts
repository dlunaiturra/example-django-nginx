import {UserModel} from "./user.model";
import {DischargeTypeModel} from "./discharge.type.model";
import {ServiceTypeModel} from "./service.type.model";
import {DischargeLocationModel} from "./discharge.location.model";
import {LocationModel} from "./location.model";

export class MaintenanceClaimModel {

    public id: number;
    public claim_date: string;
    public state: string;
    public comments: string;
    public claim: number;
    public location: DischargeLocationModel;
    public user_confirm: UserModel;
    public user_pilot: UserModel;
    public discharge_type: DischargeTypeModel;
    public service_type: ServiceTypeModel;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.claim_date = data['claim_date'];
        this.state = data['state'];
        this.comments = data['comments'];
        this.claim = data['claim'];
        this.user_confirm = new UserModel(data['user_confirm']);
        this.user_pilot = new UserModel(data['user_pilot']);
        this.discharge_type = new DischargeTypeModel(data['discharge_type']);
        this.service_type = new ServiceTypeModel(data['service_type']);
        this.location = new DischargeLocationModel(data['location']);
    };

    public initDefaultValues = () => {
        this.user_confirm = new UserModel();
        this.user_pilot = new UserModel();
        this.discharge_type = new DischargeTypeModel();
        this.service_type = new ServiceTypeModel();
        this.location = new DischargeLocationModel();
    };


}
