export interface ModelInterface {
    initValues(data?: object);

    initDefaultValues();

    getCurrentDate(): any;
}
