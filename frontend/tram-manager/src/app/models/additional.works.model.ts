import {UserModel} from './user.model';
import {ProposalModel} from './proposal.model';
import {ForecastModel} from './forecast.model';
import {AccountAssignmentHoursModel} from './account.assignment.hours.model';
import {ModelInterface} from './model.interface';


export class AdditionalWorksModel implements ModelInterface {

    public id: number;
    public name: string;
    public request_num: number;
    public request_date: string;
    public description: string;
    public proposal_main_file: File; // TODO: verificar com enviar fitxers a l'api

    public claimant: UserModel;

    public priority: string; // CHOICES
    public network: string; // CHOICES
    public application_area: string; // CHOICES

    // PROPOSAL
    public proposal: ProposalModel;
    // FORECAST
    public forecast: ForecastModel;
    // ASSIGNMENT HOURS
    public account_assignment_hours: AccountAssignmentHoursModel[];
    // BUCKET HOURS
    public bucket_assignment_hours: AccountAssignmentHoursModel[];

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    }

    initValues(data?: object) {
        Object.assign(this, data)
    }

    initDefaultValues() {
        this.claimant = new UserModel();
        this.proposal = new ProposalModel();
        this.forecast = new ForecastModel();
        this.account_assignment_hours = [new AccountAssignmentHoursModel(), new AccountAssignmentHoursModel()];
        this.bucket_assignment_hours = [new AccountAssignmentHoursModel(), new AccountAssignmentHoursModel()];
        this.request_date = this.getCurrentDate();
    }

    getCurrentDate = (): any => {
        return new Date().toISOString().split('T')[0];
    }


}

