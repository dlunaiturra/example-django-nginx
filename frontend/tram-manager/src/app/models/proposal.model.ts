import {UserModel} from './user.model';
import {PriceModel} from './price.model';
import {ModelInterface} from './model.interface';

export class ProposalModel implements ModelInterface {

    public id: number;
    public proposal_str_id: string;
    public proposed_acceptance: string;
    public comments: string;
    public proposal_file: string; // TODO: verificar com enviar fitxers a l'api
    public work_planning_file: string; // TODO: verificar com enviar fitxers a l'api
    public internal_documentation_file: string; // TODO: verificar com enviar fitxers a l'api
    public offer_request_file: string; // TODO: verificar com enviar fitxers a l'api
    public order_file: string; // TODO: verificar com enviar fitxers a l'api
    public delivery_date: string; // Date
    public approval_date: string; // Date
    public proposed_or_priority_date: string; // Date
    public maintainer: UserModel;
    public validator_user: UserModel;
    public prices: PriceModel[];

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    }

    initValues(data?: object) {
        console.log(data);
    }

    initDefaultValues() {
        this.maintainer = new UserModel();
        this.proposed_or_priority_date = this.getCurrentDate();
        this.delivery_date = this.getCurrentDate();
        this.proposed_or_priority_date = this.getCurrentDate();
        this.validator_user = new UserModel();
        this.prices = [];

        for (let parent_group of ['Materiales/Subcontratación', 'MO']) {
            this.prices.push(new PriceModel({parent_group: parent_group}));
            this.prices.push(new PriceModel({parent_group: parent_group}));
        }
    }

    /**
     * Set current date
     */
    getCurrentDate = (): any => {
        return new Date().toISOString().split('T')[0];
    }
}
