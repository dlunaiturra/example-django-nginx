import {DischargeLocationModel} from "./discharge.location.model";
import {LocationModel} from "./location.model";
import {StationModel} from "./station.model";

export class DischargeLocationClaimantModel extends DischargeLocationModel {

    public location: LocationModel;
    public start_station: StationModel;
    public end_station: StationModel;

    constructor(data?: object) {
        super(data);
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.location = new LocationModel(data['location']);
        this.start_station = new StationModel((data['start_station']));
        this.end_station = new StationModel(data['end_station'])
    };

    public initDefaultValues = () => {
        this.location = new LocationModel();
        this.start_station = new StationModel();
        this.end_station = new StationModel();
    };
}
