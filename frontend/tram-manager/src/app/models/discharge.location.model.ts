export class DischargeLocationModel {

    public id: number;
    public instalation: string;
    public catenary: boolean;
    public multitube: boolean;
    public v1: boolean;
    public v2: boolean;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.instalation = data['instalation'];
        this.catenary = data['catenary'];
        this.multitube = data['multitube'];
        this.v1 = data['v1'];
        this.v2 = data['v2'];
    };

    public initDefaultValues = () => {
        this.catenary = false;
        this.multitube = false;
        this.v1 = false;
        this.v2 = false;
    };
}
