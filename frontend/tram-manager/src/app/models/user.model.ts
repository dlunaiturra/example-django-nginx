import {ProfileModel} from "./profile.model";


export class UserModel {

    public id: number;
    public profile: ProfileModel;
    public first_name: string;
    public last_name: string;
    public email: string;
    public complete_name: string;
    public groups: Array<string>;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.complete_name = data['complete_name'];
        this.first_name = data['first_name'];
        this.last_name = data['last_name'];
        this.email = data['email'];
        this.groups = new Array<string>(data['groups']);
        this.profile = new ProfileModel(data['profile']);
    };

    public initDefaultValues = () => {
        this.profile = new ProfileModel();
    };
}
