import {CompanyModel} from "./company.model";


export class ServiceScopeModel {

    public id: number;
    public company: CompanyModel;
    public operator: string;
    public maintenance: string;
    public number_services: number;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.company = new CompanyModel(data['company']);
        this.operator = data['operator'];
        this.maintenance = data['maintenance'];
        this.number_services = data['number_services'];
    };

    public initDefaultValues = () => {
        this.company = new CompanyModel();
    };
}
