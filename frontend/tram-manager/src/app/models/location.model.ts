export class LocationModel {

    public id: number;
    public name: string;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.name = data['name'];
    };
}
