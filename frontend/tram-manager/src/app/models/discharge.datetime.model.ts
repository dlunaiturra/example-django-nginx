export class DischargeDatetimeModel {

    public id: number;
    public discharge_date: string = new Date().toLocaleDateString('en-GB');
    public start_time: string;
    public end_time: string;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.discharge_date = data['discharge_date'];
        this.start_time = data['start_time'];
        this.end_time = data['end_time'];
    };
}
