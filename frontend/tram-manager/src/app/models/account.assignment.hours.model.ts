import {ModelInterface} from './model.interface';

export class AccountAssignmentHoursModel implements ModelInterface {

    public name: string;
    public material_rodante: number;
    public infra: number;

    constructor(data?: object) {
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    }

    initValues(data?: object) {
        if (data) {
            Object.assign(this, data)
        }
    }

    initDefaultValues() {
        this.material_rodante = 0;
        this.infra = 0;
    }

    getCurrentDate(): any {
    }
}
