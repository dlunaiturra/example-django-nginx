export class ServiceTypeModel {

    public id: string;
    public type: string;

    constructor(data?: object){
        if (data) {
            this.initValues(data);
        }
    };

    public initValues = (data) => {
        this.id = data['id'];
        this.type = data['type'];
    };
}
