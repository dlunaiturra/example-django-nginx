export class ItemMenuModel {

    public name: string;
    public url: string;
    public id: string;
    public children: ItemMenuModel[];
    public active: boolean;

    constructor(data: object) {
        this.name = data['name'];
        this.url = data['url'];
        this.id = data['id'];
        this.children = new Array();
        this.active = true;
        if (data['children']) {
            this.initChildren(data['children']);
        }
    }

    private initChildren = ( children ) => {
        for(let i = 0; i < children.length; i++) {
            let item = new ItemMenuModel(children[i]);
            this.addChildren(item);
        }
    }

    public addChildren(item: ItemMenuModel) {
        this.children.push(item);
    }
}
