import {ItemMenuModel} from "./item.menu.model";

export class MenuModel {

    public items: ItemMenuModel[];

    constructor(data?: object) {
        this.items = new Array();
        if (data)
            this.initTree(data);
    }

    private initTree = (data) => {
        for(let i = 0; i < data.length; i++) {
          let item = new ItemMenuModel(data[i]);
          this.addItem(item);
        }
    }

    public addItem = (item: ItemMenuModel) => {
        this.items.push(item);
    }
}
