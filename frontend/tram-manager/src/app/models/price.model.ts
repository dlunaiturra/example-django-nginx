export class PriceModel {
    public id: number;
    public parent_group: string; // CHOICES
    public concept: string;
    public duration: number = 0;
    public price: number = 0;


    constructor(data?: object) {
        if (data) {
            Object.assign(this, data)
        }
    }
}
