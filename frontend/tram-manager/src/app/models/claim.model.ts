import {UserModel} from "./user.model";
import {ServiceScopeModel} from "./service.scope.model";
import {DischargeLocationClaimantModel} from "./discharge.location.claimant.model";
import {DischargeTypeModel} from "./discharge.type.model";
import {ServiceTypeModel} from "./service.type.model";
import {DischargeDatetimeModel} from "./discharge.datetime.model";
import {MaintenanceClaimModel} from "./maintenance.claim.model";

export class ClaimModel {

    public id: number;
    public claim_num: string;
    public claim_date: Date;
    public claimant: UserModel;
    public service: ServiceScopeModel;
    public location: DischargeLocationClaimantModel;
    public discharge_type: DischargeTypeModel;
    public service_type: ServiceTypeModel;
    public description: string;
    public discharge_datetimes: DischargeDatetimeModel[];
    public maintenance_claim: MaintenanceClaimModel;

    constructor(data?: object) {
        this.discharge_datetimes = new Array();
        this.claim_date = new Date();
        if (data) {
            this.initValues(data);
        } else {
            this.initDefaultValues();
        }
    };

    public initValues = (data) => {
        this.id = data.id;
        this.claim_num = data.claim_num;
        this.claim_date = data.claim_date;
        this.description = data.description;
        this.claimant = new UserModel(data['claimant']);
        this.discharge_type = new DischargeTypeModel(data['discharge_type']);
        this.location = new DischargeLocationClaimantModel(data['location']);
        this.service = new ServiceScopeModel(data['service']);
        this.service_type = new ServiceTypeModel(data['service_type']);
        this.maintenance_claim = new MaintenanceClaimModel(data['maintenance_claim']);
        for (var i = 0; 'discharge_datetimes' in data && i < data['discharge_datetimes'].length; i++) {
            let datetime = new DischargeDatetimeModel(data['discharge_datetimes'][i]);
            this.discharge_datetimes.push(datetime);
        }
    };

    public initDefaultValues = () => {
        this.claimant = new UserModel();
        this.service = new ServiceScopeModel();
        this.location = new DischargeLocationClaimantModel();
        this.discharge_type = new DischargeTypeModel();
        this.service_type = new ServiceTypeModel();
    };

    public newDatetime = () => {
        let datetime = new DischargeDatetimeModel();
        this.discharge_datetimes.push(datetime);
    };

    public removeDatetime = (datetime: DischargeDatetimeModel) => {
        var index = this.discharge_datetimes.indexOf(datetime);
        if (index > -1) {
            this.discharge_datetimes.splice(index, 1);
        }
    };

}
