import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';
import {Observable} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';
import {tap} from 'rxjs/operators';
import {urls} from '../urls';


@Injectable()
export class ConnectionService {

    public localhost = '/api/';
    public httpOptions = {};

    public apis = {
        login: 'login/',
        profile: 'profiles/users/me/',
        claims_list: 'claims/claims/',
        claim: 'claims/claim/',
        users_list: 'profiles/users/',
        companies_list: 'claims/companies/',
        locations_list: 'claims/locations/',
        stations_list: 'claims/stations/',
        additional_works: 'claims/works/'
    };

    public errors = {
        error_401: 401
    };

    private token = 'csrftoken';
    public tokenValue = '';

    constructor(private request: HttpClient,
                private router: Router,
                private spinner: NgxSpinnerService,
                private cookieService: CookieService) {
        this.updateToken();
    }

    private updateToken = () => {
        this.tokenValue = this.cookieService.get(this.token);
        const headers = new HttpHeaders({
                'X-CSRFToken': this.tokenValue,
                'Authorization': 'Token ' + this.tokenValue,
                'X-Requested-With': 'Fetch',
                'Content-Type': 'application/json'
            }
        );
        this.httpOptions = {
            headers,
            withCredentials: true,
        };
    }

    /**
     * Set cookie
     * @param cookieName
     * @param cookie: object
     */
    public setCookie = (cookieName: string, cookie) => {
        try {
            this.cookieService.put(cookieName, cookie);
            if (cookieName === this.token) {
                this.updateToken();
            }
        } catch (err) {
            console.log("ERROR SET COOKIE: ", err)
        }
    }

    /**
     * Get cookie and return new model object
     * @param cookie
     */
    public getCookie = (cookie: string) => {
        try {
            return this.cookieService.getObject(cookie);
        } catch (err) {
            return null;
        }
    }

    private event = (response) => {
        this.spinner.hide();
    }

    private error = (response) => {
        if (response.status === this.errors.error_401) {
            this.router.navigate([urls.login]);
        }
        this.spinner.hide();
    }

    /**
     * GET HTTP Request
     *
     * @param api
     * @param params
     * @param showSpinner
     */
    public get = (api, params, showSpinner = true): Observable<any> => {
        if (showSpinner) {
            this.spinner.show();
        }
        Object.assign(this.httpOptions, params);
        const url = this.localhost + api;
        return this.request.get(url, this.httpOptions).pipe(tap(this.event, this.error)); // Repsonse
    }

    /**
     * POST HTTP Request
     *
     * @param api
     * @param params
     * @param showSpinner
     */
    public post = (api, params, showSpinner = true): Observable<any> => {
        if (showSpinner) {
            this.spinner.show();
        }

        console.log("HTTP OPTIONS: ", this.httpOptions);
        const url = this.localhost + api;
        return this.request.post(url, params, this.httpOptions).pipe(tap(this.event, this.error)); // Repsonse
    }

    /**
     * PUT HTTP Request
     *
     * @param api
     * @param params
     * @param showSpinner
     */
    public put = (api, params, showSpinner = true): Observable<any> => {
        if (showSpinner) {
            this.spinner.show();
        }
        const url = this.localhost + api;
        return this.request.put(url, params, this.httpOptions).pipe(tap(this.event, this.error));  // Repsonse
    }

    /**
     * OPTIONS HTTP Request
     *
     * @param api: Endpoint
     * @param params:
     * @param showSpinner:
     */
    public options = (api, params, showSpinner = true): Observable<any> => {
        if (showSpinner) {
            this.spinner.show();
        }
        const url = this.localhost.concat(api);
        return this.request.options(url, params).pipe(tap(this.event, this.error));  // Repsonse
    }
}
