import {Injectable} from '@angular/core';
import {UserModel} from "../models/user.model";
import {AdditionalWorksModel} from "../models/additional.works.model";
import {GeneralDataService} from "./general.data.service";


@Injectable({
    providedIn: 'root'
})

export class AdditionalWorksService {


    public works: AdditionalWorksModel;

    constructor(private generalData: GeneralDataService) {
    }


    public setAdditionalWorks = (work: AdditionalWorksModel) => {
        this.works = work;
    }

    /**
     * Set current user and fill all related fields
     */
    public setUserData = (user: UserModel) => {
        this.works.claimant = user;
        this.generalData.getUsers();
    }

    /**
     * Set current maintenance user and fill all related fields
     */
    public setMaintainerClaimant = (user: UserModel) => {
        this.works.proposal.maintainer = user;
        this.generalData.getUsers();
    }

    /**
     * Set current pilot user and fill all related fields
     */
    public setValidatorClaimant = (user: UserModel) => {
        this.works.proposal.validator_user = user;
        this.generalData.getUsers();
    }

    /**
     * Set current pilot user and fill all related fields
     */
    public setForecastClaimant = (user: UserModel) => {
        this.works.forecast.claimant = user;
        this.generalData.getUsers();
    }
}
