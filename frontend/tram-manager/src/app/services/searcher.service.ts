import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SearcherService {

    public search: Function;
    public next: Function;
    public nextUrl: string;
    public nextOffset: number;
    public previous: Function;
    public previousUrl: string;
    public previousOffset: number;
    public filter = {name: ''};
    public counter: number;
    public limit: number;
    public offset: number;

    public update = (counter: number, nextUrl: string, previousUrl: string) => {
        this.nextUrl = nextUrl;
        this.previousUrl = previousUrl;
        this.counter = counter;
        this.updateOffset();
        this.updateLimit();
        this.updateFilters();
    };

    public updateOffset = () => {
        this.nextOffset = undefined;
        this.previousOffset = undefined;
        if (this.nextUrl) {
            const url = new URL(this.nextUrl);
            this.nextOffset = parseInt(url.searchParams.get('offset'));
        }
        if (this.previousUrl) {
            const url = new URL(this.previousUrl);
            this.previousOffset = parseInt(url.searchParams.get('offset'));
        }
    };

    public updateLimit = () => {
        this.limit = undefined;
        if (this.nextUrl || this.previousUrl) {
            const path = (this.nextUrl) ? this.nextUrl : this.previousUrl;
            const url = new URL(path);
            this.limit = parseInt(url.searchParams.get('limit'));
        }
    };

    public updateFilters = () => {
        delete this.filter['limit'];
        if (this.limit) {
            this.filter['limit'] = this.limit;
        }
    };

    public initPagination = () => {
        delete this.filter['limit'];
        delete this.filter['offset'];
    };

}
