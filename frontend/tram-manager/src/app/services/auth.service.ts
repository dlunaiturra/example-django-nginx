import {UserModel} from '../models/user.model';
import {Injectable} from '@angular/core';
import {CookieService} from 'angular2-cookie/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private keyName = 'current_user';
    private tokenName = 'csrftoken';

    constructor(private cookieService: CookieService) {
    }

    public getToken = () => {
        return this.cookieService.get(this.tokenName);
    }

    public clearData = () => {
        this.cookieService.removeAll();
    }

    public logout = () => {
        this.cookieService.removeAll();
    }

}
