import {UserModel} from '../models/user.model';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {CompanyModel} from '../models/company.model';
import {LocationModel} from '../models/location.model';
import {StationModel} from '../models/station.model';
import {ConnectionService} from './connection.service';
import {map, startWith} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class GeneralDataService {

    private usersOptions: UserModel[] = [];
    public usersControl: FormControl;
    public usersFiltered: Observable<UserModel[]>;

    public maintainersControl: FormControl;
    public maintainersFiltered: Observable<UserModel[]>;

    public validatorControl: FormControl;
    public validatorFiltered: Observable<UserModel[]>;

    public forecastClaimantControl: FormControl;
    public forecastClaimantFiltered: Observable<UserModel[]>;

    private companiesOptions: CompanyModel[] = [];
    public companiesControl: FormControl;
    public companiesFilteres: Observable<CompanyModel[]>;

    private locationsOptions: LocationModel[] = [];
    public locationsControl: FormControl;
    public locationsFilteres: Observable<LocationModel[]>;

    private stationOptions: StationModel[] = [];

    public startStationControl: FormControl;
    public startStationFilters: Observable<StationModel[]>;

    public endStationControl: FormControl;
    public endStationFilters: Observable<StationModel[]>;

    constructor(private connection: ConnectionService) {
        this.fetchData();
        this.initFormsControl();
    }

    /**
     * Fetch data from APIS, to fill suggested values.
     */
    public fetchData = () => {
        this.getCompanies();
        this.getLocations();
        this.getStations();
        this.getUsers();
    }

    /**
     * Initialize form control
     */
    private initFormsControl = () => {
        this.initCompanyControl();
        this.initLocationControl();
        this.initStationControl();
        this.initUserControl();
    }

    /**
     *
     */
    private initLocationControl = () => {
        this.locationsControl = new FormControl();
        this.locationsFilteres = this.locationsControl.valueChanges.pipe(
            startWith(''),
            map(location => this.locationFilter(location))
        );
    }

    /**
     *
     */
    private initStationControl = () => {
        this.startStationControl = new FormControl();
        this.endStationControl = new FormControl();

        this.startStationFilters = this.startStationControl.valueChanges.pipe(
            startWith(''),
            map(station => this.stationFilter(station, this.stationOptions))
        );

        this.endStationFilters = this.endStationControl.valueChanges.pipe(
            startWith(''),
            map(station => this.stationFilter(station, this.stationOptions))
        );
    }

    /**
     *
     */
    private initCompanyControl = () => {
        this.companiesControl = new FormControl();
        this.companiesFilteres = this.companiesControl.valueChanges.pipe(
            startWith(''),
            map(company => this.companyFilter(company))
        );
    }

    /**
     *
     */
    private initUserControl = () => {
        this.usersControl = new FormControl();
        this.usersFiltered = this.usersControl.valueChanges.pipe(
            startWith(''),
            map(user => this.userFilter(user))
        );

        this.maintainersControl = new FormControl();
        this.maintainersFiltered = this.maintainersControl.valueChanges.pipe(
            startWith(''),
            map(maintainer => this.userFilter(maintainer))
        );

        this.validatorControl = new FormControl();
        this.validatorFiltered = this.validatorControl.valueChanges.pipe(
            startWith(''),
            map(maintainer => this.userFilter(maintainer))
        );

        this.forecastClaimantControl = new FormControl();
        this.forecastClaimantFiltered = this.forecastClaimantControl.valueChanges.pipe(
            startWith(''),
            map(maintainer => this.userFilter(maintainer))
        );
    }

    /**
     *
     * @param value
     */
    private locationFilter = (value: string): LocationModel[] => {
        return this.locationsOptions.filter(loc => loc.name.toLowerCase().includes(value.toLowerCase()));
    }

    /**
     *
     * @param value
     * @param options
     */
    private stationFilter = (value: string, options: StationModel[]): StationModel[] => {
        return options.filter(station => station.name.toLowerCase().includes(value.toLowerCase()));
    }

    /**
     *
     * @param value
     */
    private companyFilter = (value: string): CompanyModel[] => {
        return this.companiesOptions.filter(company => company.name.toLowerCase().includes(value.toLowerCase()));
    }

    /**
     * Filter users list by input value
     *
     * @param value: Filter paremeter
     */
    private userFilter = (value: string): UserModel[] => {
        return this.usersOptions.filter(user => user.complete_name.toLowerCase().includes(value.toLowerCase()));
    }

    /**
     *
     */
    public getCompanies = () => {
        this.connection.get(this.connection.apis.companies_list, {}, false).subscribe(
            res => {
                this.companiesOptions = [];
                for (let company of res) {
                    this.companiesOptions.push(new CompanyModel(company));
                }
            },
            err => console.log(err)
        );
    }

    /**
     *
     */
    public getLocations = () => {
        this.connection.get(this.connection.apis.locations_list, {}, false).subscribe(
            res => {
                this.locationsOptions = [];
                for (let location of res) {
                    this.locationsOptions.push(new LocationModel(location));
                }
            },
            err => console.log(err)
        );
    }

    /**
     *
     */
    public getUsers = () => {
        this.connection.get(this.connection.apis.users_list, {}, false).subscribe(
            res => {
                this.usersOptions = [];
                for (let user of res) {
                    this.usersOptions.push(new UserModel(user));
                }
            },
            err => console.log(err)
        );
    }

    /**
     *
     */
    public getStations = () => {
        this.connection.get(this.connection.apis.stations_list, {}, true).subscribe(
            res => {
                this.stationOptions = [];
                for (let station of res) {
                    this.stationOptions.push(new StationModel(station));
                }
            },
            err => console.log(err)
        );
    }

    /**
     * Get POST actions and required fields
     * endpoint: /api/profiles/users
     */
    private getUserActions = () => {
        this.connection.options(this.connection.apis.users_list, {}, false).subscribe(
            res => console.log(res.actions),
            err => console.log(err)
        );
    }

    /**
     * Get POST actions and required fields
     * endpoint: /api/claims/stations
     */
    private getStationActions = () => {
        this.connection.options(this.connection.apis.stations_list, {}, false).subscribe(
            res => console.log(res.actions),
            err => console.log(err)
        );
    }
}
