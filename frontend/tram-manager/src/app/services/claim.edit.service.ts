import {Injectable} from '@angular/core';
import {UserModel} from '../models/user.model';
import {ConnectionService} from './connection.service';
import {ClaimModel} from '../models/claim.model';

@Injectable({
    providedIn: 'root'
})
export class ClaimEditService {

    public claim;

    constructor(private connection: ConnectionService) {
    }

    public setClaim = (claim: ClaimModel) => {
        this.claim = claim;
    }

    /**
     * Set current user and fill all related fields
     */
    public setUserData = (user: UserModel) => {
        this.claim.claimant = user;
    }

    /**
     * Set current maintenance user and fill all related fields
     */
    public setMaintenanceUserData = (user: UserModel) => {
        this.claim.maintenance_claim.user_confirm = user;
    }

    /**
     * Set current pilot user and fill all related fields
     */
    public setPilotUserData = (user: UserModel) => {
        this.claim.maintenance_claim.user_pilot = user;
    }


}
