import {Injectable} from '@angular/core';
import {UserModel} from '../models/user.model';
import {ConnectionService} from './connection.service';

@Injectable()
export class ProfileService {

    public user: UserModel;
    private cookie: string = 'current_user';

    constructor(private conn: ConnectionService) {
        this.getUserProfileData();
    }

    /**
     * Get personal data connected user.
     * endpoint: /profile/users/me/
     */
    private getUserProfileData = (): any => {
        this.conn.get(this.conn.apis.profile, {}, false).subscribe(
            res => {
                this.conn.setCookie(this.cookie, JSON.stringify(res));
            },
            err => {
                console.log(err);
            }
        );
    }

    public getUserProfileStoredData = (): any => {
        return new UserModel(this.conn.getCookie(this.cookie));
    }
}
