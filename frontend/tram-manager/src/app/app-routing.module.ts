import {NgModule} from '@angular/core';
import {LoginComponent} from './login/components/login/login.component';
import {Routes, RouterModule} from '@angular/router';
import {ClaimSearcherComponent} from './claims/components/claim-searcher/claim-searcher.component';
import {ClaimEditComponent} from './claims/components/claim-edit/claim-edit.component';
import {urls} from './urls';
import {AdditionalWorksComponent} from './claims/components/additional-works/additional-works.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
    {path: urls.login, component: LoginComponent},
    {path: urls.basic, component: ClaimSearcherComponent, canActivate: [AuthGuard]},
    {path: urls.claims_list, component: ClaimSearcherComponent, canActivate: [AuthGuard]},
    {path: urls.claim, component: ClaimEditComponent, canActivate: [AuthGuard]},
    {path: urls.claim + '/:id', component: ClaimEditComponent, canActivate: [AuthGuard]},
    {path: urls.works, component: AdditionalWorksComponent, canActivate: [AuthGuard]},
    {path: urls.work + '/:id', component: AdditionalWorksComponent, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
